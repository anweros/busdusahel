// Set up
var express  = require('express');
var app      = express();                               // create our app w/ express
var mongoose = require('mongoose');                     // mongoose for mongodb
var morgan = require('morgan');             // log requests to the console (express4)
var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
var cors = require('cors');
var util = require('util')

// Configuration
mongoose.connect("mongodb+srv://aymen:aymen@cluster0-rylbe.mongodb.net/test?retryWrites=true", { useNewUrlParser: true }, function(err){
  if (err){
    console.log(err)
  }
  else{
    console.log("db connection etabli...")
  }

});

app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());
app.use(cors());

app.use(function(req, res, next) {
   res.header("Access-Control-Allow-Origin", "*");
   res.header('Access-Control-Allow-Methods', 'DELETE, PUT');
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
   next();
});

var UserSchema = new mongoose.Schema({
  Id: String,
  userId: String,
  Nom: String,
  Prenom: String,
  DateDeNaissance: String,
  email: String,
  role: String
});

// Models
var utilisateur = mongoose.model('BusDuSahel',UserSchema);
// Routes

    // Get reviews
    app.get('/api/users', function(req, res) {
      /* utilisateur.find({},(err,doc)=> {
        if(err){
          console.error(err);
        }else{
          res.json(doc);
        }}) */ 
  var dbo = mongoose.connection.useDb("BusDuSahel");
  dbo.collection("BusDuSahel").findOne({}, function(err, result) {
    if (err) throw err;
    console.log(result.name);
  });
      console.log("fetching users");
    });

app.listen(4000,() => {
  console.log('serveur démarré au port 4000...');
})
